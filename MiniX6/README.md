# MINIX6

![](MiniX6.1.png)
![](MiniX6.2.png)

To run the code [click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX6/index.html?ref_type=heads)

To read the code [click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/tree/main/MiniX6?ref_type=heads)


#### Describe how does/do your game/game objects work?
I have created this program inspired by the first ever game I remember playing on my slide phone Nokia N900: The iconic Snake game. 
I have tried to recreate the aestetich of how i remember it back in the days.
Overall, the game works by continuously updating the positions of the snake and food objects, responding to user input (pressing the arrow keys), and checking for collisions. It renders changes to the canvas in each frame, creating the illusion of motion and interaction.


#### Describe how you program the objects and their related attributes, and the methods in your game.
Snake Class: The Snake class encapsulates the behavior of the snake. It has attributes such as `x`, `y` for position, `xSpeed`, `ySpeed` for movement direction, `totalSegments` to keep track of the length of the snake, and `tail` to store the positions of its tail segments. Methods like `eat()` to check if the snake eats food, `death()` to check for collision with itself, `direction()` to change its movement direction, `update()`to update its position, and `show()` to display the snake on the canvas.

Food Class: The Food class represents the food items in the game. It has attributes such as `position` to store its location on the canvas and `appleImg` to store the image of the food. It has a `pickLocation()` method to randomly select a valid position for the food and a `display()` method to display the food on the canvas.


#### Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?
By using OOP principles such as modularity, encapsulation, and abstraction, the program achieves a well-structured, maintainable, and extensible codebase. It shows how it can simplify complex codes, making them more manageable and readable.

Modularity: The code is divided into two modules: Snake and Food. Each module encapsulates related functionality and data. The Snake module manages the behavior of the snake (movement, updating position, displaying), while the Food module manages the behavior of food items (picking location, displaying).

Encapsulation: Both the Snake and Food classes encapsulate their behavior and data. The Snake class encapsulates attributes such as `x`, `y`, `xSpeed`, `ySpeed`, `totalSegments`, `tail`, and methods like `direction()`, `update()`, `show()`. Similarly, the Food class encapsulates methods like `pickLocation()` and `display()`.

Abstraction: Abstraction is achieved by providing clear interfaces for interacting with the Snake and Food classes. External code can interact with these classes through methods like `direction()`, `update()`, `show()` for Snake, and methods like `pickLocation()`, `display()` for Food, without needing to know the internal implementation details of how these methods are implemented. This abstraction hides the complexity of snake movement and food handling, making it easier to use and maintain the codebase.

#### Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
In a wider cultural context, abstraction is a fundamental concept that touches various domains beyond programming. For example, consider the design of a car. A car is composed of numerous complex systems such as the engine, transmission, suspension, and electronics. However, to the average user, these internal details are abstracted away, and they interact with the car through simple interfaces like the steering wheel, pedals, and dashboard controls. Similarly, in this Snake game project, the complexities of managing the snake's movement, collision detection, and food placement are abstracted away behind the simple code provided by the Snake and Food objects. Players interact with the game through arrow keys, enjoying the gameplay experience without needing to understand the underlying complexities of the code. This demonstrates how abstraction enables the creation of user-friendly systems in both digital and physical contexts.