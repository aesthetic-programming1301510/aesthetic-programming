// Define global variables
let snake;
let food;
let scale = 40; // Adjust the scale/size
let score = 0; // Variable to store the score

// Setup function
function setup() { //a special function in p5.js that is called once when the program starts.
    createCanvas(800, 720); //canvas width and height
    snake = new Snake(); //Initialize a new Food object; This creates a new instance of the 'Snake' class and assigns it to variable 'snake' it initializes the snake object, setting its initial position, speed, and other properties
    food = new Food(); // Initialize a new Food object; This creates a new instance of the 'Food' class and assigns it to variable 'food' it initializes the food object, setting its initial position, and other properties
    frameRate(10); // set framerate to 10 frames per second (can affect the speed)
}

// Draw function
function draw() { //a special function in p5.js that is called repeatedly through the program.
   
    drawCheckeredBackground(); //function being called that draws the checkered background

    checkSnakeStatus(); //function being called that checks if snake eats food or dies
   
    displayGameElements(); //function being called that displays snake, food, and score
}

// Create function to draw the checkered background
function drawCheckeredBackground() { //function name
    for (let x = 0; x < width; x += scale) { // This is the outer loop, which iterates over the x-coordinates of the canvas. It starts with x equal to 0 and continues as long as x is less than the width of the canvas (width). After each iteration, x is incremented by the value of scale.
        for (let y = 0; y < height; y += scale) { //This is the inner loop, which iterates over the y-coordinates of the canvas. It starts with y equal to 0 and continues as long as y is less than the height of the canvas (height). After each iteration, y is incremented by the value of scale.
            // Alternate between two shades of green based on the row and column index
            if ((x / scale + y / scale) % 2 === 0) { //This condition checks whether the sum of x / scale and y / scale is an even number when divided by 2.
                fill('#78AB46'); // : If the condition is true, it sets the fill color to a lighter shade of green.
            } else {
                fill('#6A994E'); //  If the condition is false (meaning the sum is odd), it sets the fill color to darker shade of green.
            }
            noStroke(); // No stroke
            rect(x, y, scale, scale); // Draw a rectangle at the current position x, y with specified width (scale) and height (scale)
        }
    }
}

// Create function to check if snake eats food or dies
function checkSnakeStatus() { //function name
    if (snake.eat(food)) { //checks if snake eats food; calling eat () method of the 'snake' object and passing food as an argument.
        food.pickLocation(); //if-statement = true pickLocation()-method select new random location for food
    }
    if (snake.death()) { // checks if snake has collided with itself or walls
        console.log('Game over'); //Log 'Game over' to the consol
        displayGameOverMessage(); // call function that display game over message
        noLoop(); // Stop the game loop
    } else {
        snake.update(); //update the position of the snake
    }
}

// create function to display snake, food, and score
function displayGameElements() { 
    snake.show(); // Display the snake
    food.display(); // Display the Food

    // Display score
    fill(0); // Set text color to black
    textSize(16); // Set text size
    text('Score: ' + score, width - 100, 20); // Display the score (string + the variable score) on x= width - 100 and y= 20
}

// Create function to display game over message
function displayGameOverMessage() { //function name
    textAlign(CENTER); //center text horizontally
    textSize(32); //text size
    fill(255); //set text color to white
    text("Game Over", width / 2, height / 2 - 20); //display 'Game Over' and position  
    text("Press SPACE to try again", width / 2, height / 2 + 20); //display string and position
}

// create function to increment the score when the snake eats food
function incrementScore() { // function name
    score++; //increase the score by 1; score = score + 1
}

// create Key pressed function
function keyPressed() { //function name
    if (keyCode === UP_ARROW) { //if up arrow is pressed
        snake.direction(0, -1); // if true change snake's direction -1 on the y-axis (move upwards)
    } else if (keyCode === DOWN_ARROW) { //if down arrow is pressed 
        snake.direction(0, 1); // if true change snake's direction 1 on y-axis (move downwards)
    } else if (keyCode === RIGHT_ARROW) { //if right arrow is pressed
        snake.direction(1, 0); //if true change snake's direction 1 on x-axis (move to the right)
    } else if (keyCode === LEFT_ARROW) { //if left arrow is pressed
        snake.direction(-1, 0); //if true change snake's direction -1 on x-axis (move to the left)
    } else if (keyCode === 32) { // if space key is pressed
        if (snake.death()) { // if game is over
            resetGame(); // reset the game
        }
        score = 0; //reset the score
    }
}

// create function that reset game 
function resetGame() { //function name
    snake = new Snake(); //Initialize a new Snake object; It creates a new instance of the Snake class and assigns it to the variable snake. The new keyword is used to create a new object based on the class Snake(). This new object represents the snake in the game. 
    food = new Food(); //Initialize a new Food object; creates a new instance of the Food class and assigns it to the variable food. Again, the new keyword is used to create a new object based on the class Food(). This new object represents the food item in the game. 
    loop(); // Restart the game loop
}


