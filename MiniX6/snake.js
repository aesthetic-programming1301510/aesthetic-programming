// Define the Snake class
class Snake {
    constructor() { //constructor initializes the initial state of a Snake object.
      // Initial position and speed of the snake
      this.x = 0; //x- coordinates of the snake's head
      this.y = 0; //y-coordinate of the snake's head
      this.xSpeed = 0; //Horizontal speed of the snake 
      this.ySpeed = 0; //vertical speed of the snake
      this.totalSegments = 0; // Total number of segments in the snake's tail
      this.tail = []; // Array to store the positions of the snake's tail segments
      this.snakeImg = loadImage('snake.png'); // Preload the image of the snake's head
    }
  
    // Method to check if the snake eats food
    eat(food) {
      let distance = dist(this.x, this.y, food.position.x, food.position.y); // Calculate the distance between the snake's head and the food
      if (distance < 1) { // If the distance is less than 1 unit, the snake eats the food
        this.totalSegments++; // Increase the total segments of the snake
        incrementScore(); // Increment the score
        return true; // Return true to indicate that the snake has eaten the food
      }
      return false; // Return false if the snake hasn't eaten the food
    }
  
  
    // Method to check if the snake collides with itself
    death() {
      for (let i = 0; i < this.tail.length; i++) { //loop through each segment of the snake's tail
        // Calculate the distance between the snake's head and the tail segment
        let position = this.tail[i];
        let distance = dist(this.x, this.y, position.x, position.y);
        if (distance < 1) { // If the distance is less than 1 unit, the snake collides with itself
          console.log('Game over'); // Log 'Game over' to the console
          return true; // Return true to indicate that the snake has collided with itself
        }
      }
      return false; // Return false if the snake hasn't collided with itself
    }
  
     // Method to change the direction of the snake
     direction(x, y) {
        this.xSpeed = x; // Set the horizontal speed of the snake
        this.ySpeed = y; // Set the vertical speed of the snake
      }

    // Method to update the snake's position - tail follows accordingly to the head
    update() {
      for (let i = 0; i < this.tail.length - 1; i++) { // Move each segment of the snake's tail towards the head; substracting 1 gives the index of the last element in the array, which is one less than the length of the array.
        this.tail[i] = this.tail[i + 1]; // add a segment to the tail; "move the current segment (this.tail[i]) to the position of the segment in front of it (this.tail[i + 1])."
      }
      if (this.totalSegments >= 1) {  // If the snake has at least one segment (this.totalSegments >= 1), it adds a new segment at the head of the snake.
        this.tail[this.totalSegments - 1] = createVector(this.x, this.y); //This new segment is represented by a vector with the same position as the snake's head (this.x and this.y). This effectively extends the snake's length by one segment.
      }
      // Move the snake's head based on its speed
      this.x += this.xSpeed * scale; // Update the x-coordinate of the snake's head
      this.y += this.ySpeed * scale; // Update the y-coordinate of the snake's head

      // Constrain the snake's position to within the canvas
      this.x = constrain(this.x, 0, width - scale); // Constrain the x-coordinate; the x-coordinate of the snake's head (this.x) to a value that is within the range from 0 to (width - scale), ensuring it doesn't go beyond the canvas boundaries."
      this.y = constrain(this.y, 0, height - scale); // Constrain the y-coordinate; this.y is constrained to stay within the range from 0 to (height - scale), ensuring the snake's head doesn't go beyond the top or bottom boundaries of the canvas.
    }
    
  
    // Method to display the snake on the canvas
    show() {
      strokeWeight(1); //strokeweight around each segment of the tail
      stroke(70); //strokecolor around each segment of the tail
      fill('#22A792'); //color of each segment of the tail
      //display each segment of the snake's tail as circles
      for (let i = 0; i < this.tail.length; i++) { // the for loop iterates over each element in the tail array, from the first element (index 0) to the last element (index this.tail.length - 1) and increase the value of i by 1.
        ellipse(this.tail[i].x + scale / 2, this.tail[i].y + scale / 2, scale, scale); // draw tail segments as ellipses and calculates the yx-coordinate of the center of the circle for the current tail segment. It adds scale / 2 to adjust the position so that the circle is drawn in the center of the segment.
      }
      image(this.snakeImg, this.x, this.y, scale + 2, scale + 2); // Display snake head image
    }
    }
  
  
  