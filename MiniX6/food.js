// Define the Food class
class Food {
    constructor() { //constructor initializes the initial state of a Food object.
        // Initialize the position and image of the food object
        this.position = createVector(0, 0); // Create a vector for the position (x, y) of the food object
        this.appleImg = loadImage('apple.png'); // Load the image of the food
        this.pickLocation(); // Call the pickLocation method to set the initial position of the food
    }

    
    pickLocation() { // Method to pick a random location for the food
        // Calculate the number of columns and rows on the canvas based on the scale
        let cols = floor(width / scale);  //floor() is used to calculate the number of columns based on the scale. It ensures that the number of columns is a whole number
        let rows = floor(height / scale); //floor() is used to calculate the number of rows based on the scale. It ensures that the number of rows is a whole number
            let valid = false;

    while (!valid) {
        // Select a random position for the food
        this.position = createVector(floor(random(cols)), floor(random(rows)));
        this.position.mult(scale);

        // Check if the food position doesn't overlap with the snake's position or tail
        valid = true; // Assume the position is valid until proven otherwise
        for (let segment of snake.tail) {
            if (this.position.equals(segment)) {
                valid = false; // Food position overlaps with snake's tail
                break;
            }
        }

        // Check if the food position is within the canvas bounds
        if (this.position.x < 0 || this.position.x >= width || this.position.y < 0 || this.position.y >= height) {
            valid = false; // Food position is outside canvas bounds
        }
    }
}


    
    display() { // Method to display the food object on the canvas
        image(this.appleImg, this.position.x, this.position.y, scale, scale); // Display the image of the food at its position (width and height is represented by scale which ensures the food the same size as the variable scale)
    }
}
