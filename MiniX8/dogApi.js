let dogImages = [];
let soundtrack;
let barkSound;
let backgroundImg;
let dogNames = ["Bacon",
  "Bark Twain",
  "Bullwinkle",
  "Basse",
  "Charlie",
  "Sir Charles",
  "Elmo",
  "Furdinand",
  "Jimmy Chew",
  "Kanye Westie",
  "Kim Kardogshian",
  "Little Bow Wow",
  "Notorious D.O.G.",
  "Charlie Brown",
  "Chewbacca",
  "Chewie",
  "Einstein",
  "Elmo",
  "E.T.",
  "Hooch",
  "Little Bow Wow",
  "Oscar Mayer",
  "McGruff",
  "Meatball",
  "Meatloaf",
  "Muddy Buddy",
  "Muttley Cru",
  "Nacho",
  "Pee Wee",
  "Phideaux",
  "Porkchop",
  "Sir Barks-a-Lot",
  "Sir Licks-a-Lot",
  "Snoop Doggie Dog",
  "Subwoofer",
  "Taco",
  "Waldo",
  "Woofer",
  "Barkley",
  "Bingo",
  "Cinder Ella",
  "Lady Rover",
  "Miss Piggy",
  "Munchkin",
  "Pup Tart",
  "Slink",
  "Sushi",
  "Tater",
  "Waffles",
  "Al Poo-Chino",
  "Bark Twain",
  "Catherine Zeta Bones",
  "Chew Barka",
  "Droolius Caesar",
  "50 Scent",
  "Fresh Prints",
  "Furdinand",
  "Groucho Barks",
  "Indiana Bones",
  "Jimmy Chew",
  "Kanye Westie",
  "Mary Puppins",
  "Orville Redenbarker",
  "Salvador Dogi",
  "Sherlock Bones",
  "Spark Pug",
  "Winnie the Pooch",
  "Woofgang Amadeus",
  "Woofgang Puck",
  "Angus",
  "Bear",
  "Beethoven",
  "Boss",
  "Bruiser",
  "Brutus",
  "Button",
  "Byte",
  "Dozer",
  "Gumdrop",
  "Jellybean",
  "Jitterbug",
  "King Kong",
  "Rambo",
  "Spud",
  "Subwoofer",
  "Squirt",
  "Tank",
  "Atom",
  "Bubba",
  "Bullwinkle",
  "Butterball",
  "Chuck Norris",
  "Marshmallow",
  "Mac Daddy",
  "Napoleon",
  "Pee Wee",
  "Queen Bey",
  "The Notorious D.O.G.",
  "Mickey",
  "Minnie",
  "Balto",
  "Toffee",
  "Kakao",
  "Walter",
  "Jesse Pinkdog",
  "Walter Bite",
  "Chaos",
  "Bluey",
  "Bandit",
  "Chilli"
];

function preload() {
  // Load your sound file in the preload function
  font = loadFont('8bitFont.ttf')
  soundtrack = loadSound("backgroundmusic.mp3");
  barkSound = loadSound("dogbark.mp3");
  backgroundImg = loadImage("Dog park.PNG");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(backgroundImg);
}

function draw() {
  textSize(125);
  textAlign(CENTER, CENTER)
  textFont(font);
  fill(255);
  text("WELCOME TO THE DOGPARK", width / 2, 65);
}

function mousePressed() {
  let diameter = random(90, 150);
  let randomIndex = floor(random(dogNames.length));
  let randomString = dogNames[randomIndex];

  // Fetch a random dog image and display it at the position of the mouse click
  fetch('https://dog.ceo/api/breeds/image/random')
    .then(response => response.json())
    .then(data => {
      const img = createImg(data.message, '');
      img.hide();
      img.style('width', `${diameter}px`);
      img.style('height', `${diameter}px`);
      img.style('border-radius', '50%');
      img.position(mouseX - diameter / 2, mouseY - diameter / 2);
      img.show();
      dogImages.push(img);

      // Display the random dog name underneath the image
      textAlign(CENTER);
      textSize(25);
      textFont(font);
      fill(255);
      text(randomString, img.x + diameter / 2, img.y + diameter + 20);
      });

  // Audio context check and audio playback
  if (getAudioContext().state !== 'running') {
    getAudioContext().resume();
  }

  // Check if the soundtrack is playing, and play if it's not already playing
  if (!soundtrack.isPlaying()) {
    soundtrack.loop(); // This will make the music loop indefinitely
  }
  barkSound.play();
}

