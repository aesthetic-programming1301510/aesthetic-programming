## Welcome to the Dog Park

![](MiniX8.png)

To run the code [click here]()

To read the code [click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/tree/main/MiniX8)


#### Project Overview
Welcome to the Dog Park is an interactive application designed to entertain users by generating random dog images and names on a virtual dog park backdrop. Each time a user clicks within the app window, a new dog image appears at the click location along with a randomly selected dog name displayed below the image. This project utilizes p5.js for drawing and interaction, the Dog CEO's Dog API for fetching random dog images, and custom sound effects to enhance the user experience.
API 
Dog CEO's Dog API: Provides random images of dogs. This API was chosen for its simplicity and reliability, allowing for easy integration into our interactive setup.

#### Reflection on the Process
Data Acquisition and Usage
The main data for this project comes from the Dog CEO's Dog API, which is utilized to fetch random dog images. The process involves sending HTTP GET requests to retrieve data in JSON format, specifically the URL of a random dog image. This simplicity in data structure (URLs pointing to images) makes it easy to handle and integrate directly into our visual setup.


#### Data Representation
The data, while simple, is effectively represented visually in a playful and interactive manner. Images are not just displayed but made a part of an engaging digital environment, complemented by corresponding names from a predefined list and thematic audio elements.
Understanding and Insights
The Dog API, like many public APIs, abstracts much of the data processing and storage, delivering only the final data needed (image URLs). It's an example of how APIs can streamline app development by handling complex backend processes and allowing developers to focus on frontend experience. However, the sorting and management of this data on the API provider's server remain opaque.
Power Relations and API Significance
APIs like Dog CEO represent a power dynamic where the provider controls data access, structure, and reliability. They play a crucial role in digital culture by encapsulating functionalities and data, promoting modularity, and enabling diverse applications to be built with rich feature sets that would be hard to develop independently.

#### Questions for Further Investigation
###### If time permitted, I would like to explore the following questions further:
How do API providers manage and prioritize traffic and requests to ensure service reliability and fairness?

What are the challenges in maintaining an API from the provider's perspective, especially regarding data consistency, scaling, and security?

How can developers handle and visualize data more dynamically from multiple APIs to create more complex and data-rich applications?

#### Conclusion
This project not only showcases a fun and interactive use of web APIs but also invites users to consider the underlying technology and the data it manipulates. It opens up avenues for discussing the importance of APIs in modern digital infrastructures and the hidden complexities of seemingly simple applications.

