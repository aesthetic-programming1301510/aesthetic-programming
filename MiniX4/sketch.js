let button; //declare variable button 
let colorText; //declare variable colortext 
let scene = 0; //declare variable scene and assign it to 0

function setup(){ // core function in p5.js executed once
  createCanvas(windowWidth,windowHeight); //size on the canvas
  frameRate(100) //how many images pr second  

  //OK-knappen + styling
  button = createButton('OK'); //create button and button text
  button.style("font-size","12px"); //font size 
  button.position(700,335); //position of button (x, y)
  button.size(60,30); //size of button (width, height)
  button.style("color","black") //text color
  button.style("background-color","#42f57b"); //color of button
  button.mousePressed(sceneChange); //when button is pressed the scene change from 0 -> 1
}
  
  function draw() { //define function drawElements

  if (scene == 0){ //if-statement when the scene is 0
    background(255); //white background
    // Draw the "Cookies-box"
    push()
    noStroke ();
    fill(170) //surrounding box color - grey
    rect(575, 250, 300, 150) //surrounding box (x, y, w, h)
    stroke(0); //stroke black color
    fill(160) //upper box color - darker grey
    rect(575, 250, 300, 30) //upper box
    rect(850, 255, 20, 20) //escape box
    strokeWeight(2) //linjer  
    line(855, 260, 865, 270) //the cross in escape box
    line(865, 260, 855, 270) //the cross in escape box
    strokeWeight(1) //strokeweight of text
    fill(0) //sort text
    textSize(14) //text størrelse
    text("DO YOU WANT TO ACCEPT COOKIES",600, 310) //text x, y
    pop()

  } else if (scene == 1){ //when the scene is 1
    textSize(15); //size of the text in the background
    push();
    iphone(0,10);  //I call the functions with the background text (x, y coordinates) 
    google(0,35);
    spotify(0,60);
    mastercard(0,85);
    hund(0,110);
    thai(0,135);
    nag(0,160);
    SMS(0,185);
    tinder(0,210);
    mastercard(0,235);
    google(0,260);
    iphone(0,285);
    thai(0,310);
    spotify(0,335);
    SMS(0,360);
    hund(0,385);
    nag(0,410);
    tinder(0,435);
    spotify(0,460);
    mastercard(0,485);
    iphone(0,510);
    google(0,535);
    SMS(0,560);
    thai(0,585);
    tinder(0,610); 
    nag(0,635);
    hund(0,660);
    mastercard(0,685);
    spotify(0,710);
    pop();

   //ellipses that follows mouseXY when pressed footprints  
    push();
noStroke();
fill(0, 0, 0, 200); //the color of scene 1 (r,g,b,alpha)
if (mouseIsPressed) { //if-statement: if mouse is pressed
  translate(mouseX, mouseY); // Translate to mouseX and mouseY
  rotate(radians(30)); // Rotate by 20 degrees
  ellipse(0, 0, 30, 60); // Draw first ellipse relative to translated and rotated origin
  ellipse(0, 50, 20, 30); // Draw second ellipse relative to translated and rotated origin

} else { //else-statement: when mouse is not pressed 
  ellipse(mouseX, mouseY, 10, 10); // ellipse creating path between footprints (x, y, w, h)
}
  pop();

  }
  }

  function sceneChange(){ //creating sceneChange function 
    scene = 1; //When sceneChange is called scene = 1
  }

  function iphone(x,y){ //declare function named iphone with parameters (x,y)
    for (let x = 0; x < width; x+=315) { //for-loop: loop starts at; the loop continues as long as x < width; x = 0; Each iteration happens at 305px (at the end of each sentence) 
     fill(255); //color of text
     text("Iphone 13pro sidst logget ind kl.03.43 25.03.24", x, y); //the text + parameters x, y which is assigned when the function is called    
  }
 }
 
 function google(x,y){
  for (let x = 0; x < width; x+=275) {
    fill(255);
    text("Googlede: Hvordan taber man sig 10kg?", x, y); 
 }
 }
 
 function spotify(x,y){
 for (let x = 0; x < width; x+=260) {
  fill(255);
  text("Spotify kl.22.33: Kinky fætter /Suspekt ", x,y); 
 }
 }
 
 function mastercard(x,y){
 for (let x = 0; x < width; x+=604) {
  fill(255);
  text("Mastercard XXXX XXXX XXXX 9087, REMA1000 Dirch Passers Gade X kl.09.57 10.02.24 ", x, y); 
 }
 }
 
 function hund(x,y){
 for (let x = 0; x < width; x+=298) {
  fill(255);
  text("kl.14.04 10.01.24 Hund registreret på billede", x, y); 
 }
 }
 
 function tinder(x,y){
   for (let x = 0; x < width; x+=315) {
     fill(255);
     text("kl.15.46 20.03.24 Tinder: du har fået et match!!", x, y); 
 }
 }

 function SMS(x,y){
   for (let x = 0; x < width; x+=205) {
    fill(255);
    text("SMS modtaget: Tak for i går;))", x, y); 
 }
}

function thai(x,y){
 for (let x = 0; x < width; x+=523) {
  fill(255);
  text("her: Lulus thai massage, Marstrandsgade XX kl.20.10 12.03.23 Checkede ind", x, y); 
 }
 }

 function nag(x,y){
   for (let x = 0; x < width; x+=345) {
   fill(255);
   text("Googlede: Hedder det bære den af eller bære nag?", x, y); 
 } 
 }
