# MiniX4

![](MiniX4.png)
![](MiniX0.2.png)
![](MiniX0.3.png)

#### To run the code [click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX4/index.html)

#### To read the code [click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/tree/main/MiniX4)

## "Digital Footprints

In this program my vision is to visualize the hidden data. I delve into the complex world of digital data capture and its lack of personal privacy. Initially overlooking subtler methods like mouse tracking, I underscore the central role in shaping online experiences. Drawing parallels with cookies, I illuminate the subtle yet impactful presence of these elements in our digital lives, often operating without user awareness.
Through an interactive demonstration, the click on "OK" unveils hidden data. represented by black dots following the mouse movements, the more times you click the more underlying data will be revealed.


#### My program

I have created a webpage with a box resembling a cookies consent request.  It is possible and even encouraged to press the button "OK". When the button is pressed, the box get slightly transparent and reveals a dark page behind the box. Also when the mouse is hovered on the white webpage the cursor will leave small black dots gradually unveiling a black layer beneath the surface of the webpage. As you continue to click, footprints emerge - The more you click the more underlying data will be revealed.


I have used Mouse capture by drawing ellipses that follow mouseX and mouseY, which creates a trace of the mouse's position. However, if the mouse is clicked with mousePressed(), two ellipses resembling a footprint will appear. These two event-driven conditions are written within an if/else-statement, this causes the small ellipses to cease when mousePressed() is true, but resume when mousePressed() is false.
Focusing on the syntaxes, I've used different elements, such as DOM elements with my button, custom functions, and the mousePressed() && sceneChange() in the if-statements. Additionally, I've also used for-loops for each line of text in the background:
function iphone(x,y){    for (let x = 0; x < width; x+=315) {     fill(255);     text("Iphone 13pro sidst logget ind kl.03.43 25.03.24", x, y); 


#### Theme: "Capture All"

I find it truly unsettling that there are individuals and companies out there whom we have no idea about, yet they can profit from the information we provide. We are more or less involuntarily drawn into the realm of Big Data, where our data is regarded as a raw material for large corporations to extract and convert into economic gains. "Datafication can be seen as a new form of data colonialism that exploits human life, allowing continuous extraction of data..." (Mejias & Couldry, 2019). This can be likened to Shoshana Zuboff's concept of surveillance capitalism, where every online action and interaction is monitored and exploited for capitalist purposes. I truly find this topic to be complex and challenging. At times, I admit to seeing a slight advantage – it's quite convenient to have our feeds and advertisements tailored to our preferences. However, over the past week, I've felt incredibly surveilled online, prompting me to be more cautious about my online activities and what I agree to.
Moreover, I am fascinated by how easily we surrender our data. The role of the button here is quite significant to discuss, as the "accept" button is often larger than the "reject" button, perhaps styled with a green color while the other is red or gray. It almost instinctively urges one to click on it.
