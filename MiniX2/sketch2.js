let rotationAngleX = 0; // declare variable rotationX equal to 0
let rotationAngleY = 0; // declare variable rotationY equal to 0


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL); // Canvas size WEBGL = 3D

}

function draw() {
  background(52, 235, 195); //Backgroundcolor (cyan)
  
 
  // Calculate rotation angles based on mouse position - (the map function translates the position of mouse into rotation angles applied on sphere)
  let targetRotationAngleX = map(mouseY, 0, height, 50, -50); // map the vertical position of 'mouseY' to a rotation angle around the x-axis 'targetRotationAngleX' (value start1, stop1, start2, stop2) start1/stop1 mouse range - start2/stop2 rotation range
  let targetRotationAngleY = map(mouseX, 0, width, -50, 50); // map the horizontal position of 'mouseX' to a rotation angle around the y-axis 'targetRotationAngleY' (value start1, stop1, start2, stop2)

  // Rotation movement and pace smoother/slower
  rotationAngleX += (targetRotationAngleX - rotationAngleX) * 0.05; //the amount by which the object needs to rotate to align its x-axis orientation with the desired rotation angle dictated by the mouse position. (* 0.05 slows the movement down)
  rotationAngleY += (targetRotationAngleY - rotationAngleY) * 0.05; //the amount by which the object needs to rotate to align its y-axis orientation with the desired rotation angle dictated by the mouse position. (* 0.05 slows the movement down)

 //lighting
  ambientLight(240); //brighten up the whole scene
  directionalLight(255, 255, 255, 100, 200, 100); //light patch on top of the face (R, G, B, x, y, z)

  // Draw the smiley face
  rotateX(radians(rotationAngleX)); // Use the function rotate around the x-axis based on mouse movement
  rotateY(radians(rotationAngleY)); // Use the function rotate around the y-axis based on mouse movement

  noStroke() // No stroke on the face
  fill(255,204,51); //colour of the face (yellow)
  sphere(100, 500, 400); //size and position of sphere (radius, x, y)

  // Draw the eyes
  fill(153, 102, 51);
  translate(-25, -25, 75); // Position of the left eye (x, y, z)
  ellipsoid(10, 20, 30); // Left eye (radiusx, radiusy, radiusz)

  translate(50, 0, 0); // Position of the right eye (x, y, z) 
  ellipsoid(10, 20, 30); // Right eye (radiusx, radiusy, radiusz)

  // Draw the mouth
  fill(153, 102, 51); // Colour of mouth (dark brown)
  translate(-25, 40, 3); // Position of the mouth (x, y, z)
  rotateX(19.9); // Slightly tilt the torus on the x-axis so it looks like a smile
  torus(30, 5); // Mouth size (radius, tuberadius) width and thickness of the smile 

}




