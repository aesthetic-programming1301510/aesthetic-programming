# MiniX2

#### [Click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/blob/main/MiniX2/index.html) to read code for emoji 1

#### [Click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX2/index.html) to run the code for emoji 1  


#### [Click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/blob/main/MiniX2/index2.html) to read code for emoji 2

#### [Click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX2/index2.html) to run the code for emoji 2 

## Emoji1

![](MiniX2.0.png)

## Emoji2

![](MiniX2.1.png)

###### Describe the program you have used and what you have learned.
For this assignment, I've created what I consider to be the most neutral emoji. I've designed two emojis that look similar but exist in two different dimensions: emoji 1 in 2D and emoji 2 in 3D. Following the assignment guidelines, I've incorporated various geometric shapes such as ellipses, arcs, ellipsoids, spheres, and toruses. Most of my time was spent on the 3D version, with the 2D version created primarily for comparison purposes to highlight the differences between them.
To make the 3D version interactive, I've programmed the sphere to rotate in a specific range to follow the mouse. This creates the illusion that the smiley face is tracking the movement of the mouse cursor.

###### Would you put your emoji into a wider social and cultural context?
Emojis serve as a means of expressing emotions. In the early days of computing, we used symbols like ;) :( :O to convey feelings. However, today there are more than a thousand different emojis to choose from, and some of them are quite detailed and specific. To promote inclusivity, iOS has made it possible for you to adjust the skin color of each emoji and has created both male and female versions of each character.
Despite the attempts to be inclusive and diverse, there will always be a minority who don’t fit into the predefined categories. So, I didn’t try to create an advanced and well-thought-out emoji with the purpose of embracing every single individual in the world. Instead, I recreated and reflected upon the original smiley made of symbols :).
When the symbols are typed on Facebook, the iOS version of :) renders as 🙂. With so many options available, the original smiley doesn’t necessarily convey happiness. While it might seem neutral to some, to others, it could appear passive-aggressive or ironic compared to other smiley emojis that is more expressive.
The meaning of the emoji shifts in terms of who the sender is and in what context it is used. In that way, this emoji is one of, if not THE most neutral emoji in my opinion. But that's also why I tend to avoid using this particular emoji. It's difficult to know how people interpret it, especially since it doesn't express as many emotions as other emojis do.
So, to answer the question “Would I put it into a wider social and cultural context”, my answer is both yes and no. If both the sender and receiver share the same understanding of its meaning, the emoji doesn't cause harm. However, if this isn’t the case, then the emoji loses its purpose, and the message might have been clearer without it.
To make something complex and diverse can be more inclusive, but it can also be the source of exclusion. By keeping it simple with only a few emojis available like the OG’s symbol smiley faces, that only have one representation of each emotion like happiness, anger, surprise, etc., sets the tone of the message in a clearer way than it does today with over a thousand different possibilities that can be misinterpreted. 
