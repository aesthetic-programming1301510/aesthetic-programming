function setup() {
  createCanvas(windowWidth, windowHeight); //Canvas size
  background(255, 130, 207); //background color pink
  angleMode(DEGREES); //set angle to degrees insted of radians
  
}

function draw() {
  //Face
  fill(255, 204, 51); // color of the face (yellow)
  stroke(227, 125, 66, 500); // Color and density(250) of the stroke around the face
  strokeWeight(5); // weight of the stroke
  ellipse(150, 150, 200, 200); // placement and size of the face (x, y, W, H)

  //Eyes
  stroke(101, 67, 33, 250) // Stroke colour (dark brown)
  fill(131, 77, 43, 245); // Eye color (light brown)
  ellipse(120, 130, 20, 35); // Left eye (x, y, W, H)
  ellipse(180, 130, 20, 35); // Right eye (x, y, W, H)
 
 
  //Mouth 
  noFill(); // No fill in the arc
  strokeWeight(5); // Weight of stroke
  arc(150, 170, 90, 60, 30, 150); // arc position and size (x, y, w, h, start, stop) start/stop is in degrees 
  
}




