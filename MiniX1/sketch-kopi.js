let distMouse = 50; // The distance around the mouse that interact with the flowers
let flowers = []; // Declaration of an array that hold the flower objects
let spacing = 10; // Spacing between flowers in centimeters
let angle = 0; //The variable angle is declared and assigned 0 - this value will be used to rotate the flowers

function setup() {
  createCanvas(windowWidth, windowHeight); //The canvas fills the whole window
  rectMode(CENTER); // x, y passed to rect() represents the center of the rect
  angleMode(DEGREES); //set angle mode to degrees

  // Create flowers with spacing
  for (let y = 0; y < height;y += spacing) { // outer loop y < height loop run as long as y coordinates is less than the canvas height. y += spacing the spacing between each iteration/loop
    for (let x = 0; x < width;x += spacing) { // inner loop x < width loop run as long as y coordinates is less than the canvas height. x += spacing the spacing between each iteration/loop
      //Create a new Flower object and add it to the flowers array
      flowers.push(new Flower(x, y)); //flowers.push add Flower to flowers array.(x/y) the position based on the current values of x/y (the grid cell). 
    }
  }
}

function draw() {
  background(400, 0, 400); //Backgroundcolor (pink)
  
  // Update angle of each flower based on the mouse distance to flower position
  for (let element of flowers) { // for..of loop iterates the objects in the array "flowers". The variable "element" represents each flower object stored in the flowers array
    element.hover(mouseX, mouseY); //call the hover method on each flower based on the current mouseX and mouseY positions as arguments. This code is executed for each element in the "flowers" array
    element.display(); //call the display method on the flower object "element". Display renders the flower at its current position (exp: rotating based on the angle variable)
  }
}


// Flower class that ensures each flower object to have its own unique x and y coordinates that define the position of the flower object
class Flower { //Defines a class "Flower" representing flower objects 
  constructor(x, y) { //defines constructor method. the parameters x and y represents the initial coordinates of the flower 
    this.x = x; //this.x is a property of the flower object. The line assigngs the value of the x parameter (representing the x coordinate of the flower being created) to the x property of the flower object being created
    this.y = y; //this.y is a property of the flower object. The line assigngs the value of the y parameter (representing the y coordinate of the flower being created) to the y property of the flower object being created
    this.angle = 0; //sets the initial angle property of the flower object to 0 (used to control the rotation of the flower)
  }

  hover(mx, my) { //declares the method "hover" ith the parameters mx and my
    let distance = dist(mx, my, this.x, this.y); // Calculates the distance between the mouse position (mx, my) and the flower's position (this.x, this.y)
    if (distance < distMouse) { //A conditional statement that checks if distance is less than the value in the variable distMouse (if less then true)
      this.angle += 5; // If the statement is true (distance < Distmouse) increase the angle of the flower by 5 degrees.
    }
  }

  display() { //Defines the method "display" within the Flower class
    push(); //save the current drawing state
    translate(this.x, this.y); //"translate" function move the (0,0) of the coordinate system to the position of the flower (this.x and this.y). the method display will be performed relative to the flower's position 
    rotate(this.angle); //"rotate" function rotates the coordinate system (0,0) by the angle specified in the flower's properties 
   
    // Draw flower (construction of the flower)
    let c = color(255, 204, 0); //Defines the variable C that creates a color object with RGB (yellow)
    fill(c); //sets the fill color to the value stored in the variable
    noStroke(); //disables the stroke (outline) for shapes drawn after this command.
    rect(0, 0, 1, 50); // rectangle with the position (0,0) width and height (1, 50) units

    c = color(0, 204, 0); // changes the value of variable c to green
    fill(c); //sets the fill color
    rotate(40); //Rotate drawing operations with 40 degrees
    rect(0, 0, 1, 50); // rectangle

    c = color(0, 0, 204); //color blue
    fill(c); //fill in color
    rotate(50); //Rotate drawing operations with 50 degrees
    rect(0, 0, 1, 100); //rectangle

    c = color(0, 204, 204); //color cyan
    fill(c); //fill in color
    rotate(50);//Rotate drawing operations with 50 degrees
    rect(0, 0, 1, 100); // rectangle
    pop(); //Restore the previous drawing state saved by push(). It undoes any transformation (like rotations applied after the last push()
  }
}


