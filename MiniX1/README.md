# MiniX1

#### The rendered version of my code
![](Minix1.png)

#### To run the code click [here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX1/index-kopi.html?ref_type=heads)

#### To see the code click [here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/tree/main/MiniX1?ref_type=heads)

## What I have produced and my thoughts about the process
I created a webpage featuring a flower design made of small rectangles. When hovering over the flowers within a 50-pixel radius, they rotate and form patterns on the page.

Working on the assignment without guidelines is liberating yet challenging, especially for a programming novice like myself. My aim was to produce something impressive, but I needed to grasp the basics first. To kickstart, I tried finding inspiration on YouTube through p5.js tutorials to see what beginners could achieve. Inspired by a video demonstrating spinning rectangles, I initially attempted to create a windmill with three spinning rectangles. However, that didn't work out as planned, leading me to the create flowers instead.

Throughout the process, I continuously refined the design—adjusting petal lengths, quantities, and colors. Experimentation even led me to change the number of flowers on the webpage, ultimately resulting in a final design that resembles more of a whiteboard (or cyan-board) capable of generating colorful patterns when hovered over with the mouse.
Despite the unconventional outcome, I enjoyed the assignment. While my design may not have practical applications, the experience allowed me to fearlessly experiment with coding. Without the pressure of meeting specific requirements, I found the entire journey both joyful and educational.
