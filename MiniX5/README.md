# MiniX5

![](MiniX5.1.png)
![](MiniX5.2.png)

#### To run the code [click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX5/index.html)

#### To read the code [click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/tree/main/MiniX5)

#### My program
My code generates a dynamic pattern of perlin noise, where both colors and patterns change. The color is set to only blue and white shades to resemble a stormy sea. The pattern and color change is mimicking the various colors and wave movements found in the sea, creating a representation of its dynamic nature.
The continuous updates of the positions of particles based on Perlin noise, results in fluid, dynamic movements across the canvas. Additionally, the particles periodically change color, adding visual variety to the display.

Perlin Noise: The use of Perlin noise generates smooth, natural-looking movements for the particles, creating complex patterns and movements resembling the sea.

Color Changes: Changes in particle color introduce variability and aesthetic appeal to the visualization.

Boundary Conditions: Particles reset their positions if they move off-screen, leading to an ever-changing composition of particles within the canvas bounds.

###### The code is built around the for loops and conditional statements:

Inside the `setup()` function, the for-loop `for (let i = 0; i < numParticles; i++)` is used to initialize the particles array with random positions for each particle.

Inside the `draw()` function, another for-loo `for (let i = 0; i < numParticles; i++)` is used to update the positions of particles and draw them on the canvas.

I also have several if-statements:
One if-statement if (!isOnScreen(particle)` is used to check if the particle is off-screen and reset its position if so.

Similarly, if-statement `if (millis() - lastColorChangeTime >= changeInterval)` is used to check if it's time to change the particle color.

Another  if-statement `if (millis() - lastNoiseChangeTime >= changeInterval)` is used to check `f it's time to change the noiseSeed.


The code exemplifies the concept of an "auto-generator" by demonstrating how rules and processes can autonomously generate dynamic visual patterns. Drawing upon the assigned reading, particularly the idea of "Levels of Control” For instance, the rules that control particle movement using Perlin noise provide a high level of autonomy, allowing for complex and unpredictable patterns to emerge. But, the program also has controlled processes such as periodic color changes and boundary conditions, which introduce a level of human intervention and guidance.
