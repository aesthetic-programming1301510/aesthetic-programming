let particles = []; //// Declare an array to store particle vectors
const numParticles = 100000; // Define the number of particles in the system
let particleColor; // Variable to store particle color
let lastNoiseChangeTime = 0; // Variable to keep track of the last time noiseSeed was changed
let lastColorChangeTime = 0; // Variable to keep track of the last time particle color was changed
const changeInterval = 20000; // Interval for changes (colors and noiseSeed) in milliseconds

const noiseScale = 0.01; //scale factor for the noise function. Used to control the frequency and smoothness of the generated noise (lower value = smoother/slower paths/movements)

function setup() { // Setup function, runs once at the beginning
  createCanvas(windowWidth, windowHeight); // Create a canvas that fills the window
  for (let i = 0; i < numParticles; i++) { // loop counter "i" start from 0; Condition for the loop execute as long as i<numParticles; Increment the loop counter after each iteration.
    particles.push(createVector(random(width), random(height))); //Within the loop a new particle vector is made 'createVector()' with random x and y but within canvas dimension (width and height) - the new particle vector is added into the array 'particles' with the push.
  }
  particleColor = color(255); // Initialize particle color to white
}

function draw() { // Draw function, runs continuously after setup
  background(0, 10); // Set background color to black and use alphavalue as transparencyeffect (This makes the particles look like lines instead of dots)
  stroke(particleColor); // To color a point (which is used in the line underneath), use the stroke() to color.
  for (let j = 0; j < numParticles; j++) { // The for loop is used to continuously update the positions of particles and draw them on the canvas.
    let particle = particles[j]; // Retrieves the particle at the current index 'i' and assigns it to the variable particle.
    point(particle.x, particle.y); // Draw the particle as a single point on x- and y-coordinates.
    let noiseValue = noise(particle.x * noiseScale, particle.y * noiseScale); // Declare a noiseValue based on the particle's position that is decided by particle.x.y and noiseScale.
    let angle = TAU * noiseValue; // Calculate an angle based on the noise value TAU=2pi(full circle). It adds randomness and variability to the particle's motion.
    let velocity = createVector(cos(angle), sin(angle)); // Create a velocity vector based on the angle. is used to create a velocity vector for each particle. This velocity vector determines the direction of movement for the particle on the canvas.
    particle.add(velocity); // Move the particle according to its velocity. It updates the position of the particle by adding the velocity vector to its current position, effectively moving the particle in the direction and with the speed specified by the velocity vector.
   
    if (!isOnScreen(particle)) { // If the particle is not on the screen, reset its position
      particle.x = random(width); // then reset to random x position
      particle.y = random(height); //then reset to random y position
    }
  }
  
  // Check if it's time to change noiseSeed
  if (millis() - lastNoiseChangeTime >= changeInterval) { //The if statement checks if the difference between the current time (given by millis()) and the time when the noise seed was last changed (lastNoiseChangeTime) is greater than or equal to the change interval (changeInterval).
    changeNoiseSeed(); //if true NoiseSeed will change leading to different Perlin noise pattern.
    lastNoiseChangeTime = millis(); //if true update the lastNoiseChangeTime variable to current time (resetting time)
  }
  
  
  if (millis() - lastColorChangeTime >= changeInterval) { //The if statement checks if the difference between the current time (given by millis()) and the time when the particle color was last changed (lastColorChangeTime) is greater than or equal to the change interval (changeInterval).
    changeParticleColor(); //if true change the color of the particles
    lastColorChangeTime = millis(); //if true  updates the lastColorChangeTime variable to the current time, resetting the timer for the next color change.
  }
}

function isOnScreen(vector) { //function isOnScreen() takes a vector as input and checks if it is within the boundaries of the canvas.
  return vector.x >= 0 && vector.x <= width && vector.y >= 0 && vector.y <= height; //return boolean value(true) if vector.x.y is greater or equal to 0 and is less or equal to height/width
}

function changeNoiseSeed() { //create function that is responsible for updating the noiseSeed used in generation perlion noise
  noiseSeed(millis()); //When noiseSeed is called, the noiseSeed is updated/changed to a different value based on the current time
}

function changeParticleColor() { //Create function that is responsible for changing /updating colors of the particles 
  particleColor = color(random(0, 50), random(0, 200), random(200, 255)); // When called changeParticleColor() creates a new color by randomly selecting values for the (Red 0-50), Green(0,200), and Blue(200-255) components within the specified ranges.

}
