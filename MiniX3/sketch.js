let rectangleColor; // Declare variable 'rectangleColor' 

function setup() { // core function in p5.js executed once
  createCanvas(windowWidth, windowHeight); //Define canvas size
  frameRate(300); //Set framerate for animation (increasing makes the animation smother)
  rectangleColor = color(0, 255, 0); // Assign color green to rectangleColor with color(R,G,B)
}

function draw() { // core function in p5.js that is called automatically in a loop
  background(30, 5); //Background color and alpha value (transparency)
  drawElements(); //Call the function to draw element
}

function drawElements() { //define function drawElements
  let number = 400; //Define the number of rectangles to be drawn
  
  translate(width / 2, height / 2); //translate origin to center of the canvas
  //360/num >> degree of each ellipse's movement;
  //frameCount%num >> get the remainder that to know which one
  //among 8 possible positions.
  let rectangle = 360 / number * (frameCount % number); //calculate the angle of rotation for each rectangle based on the framerate
  rotate(radians(rectangle)); //rotate the canvas by the calculated angle
  noStroke(); // no stroke on rectangles
  fill(rectangleColor); // Set the fill color of the rectangles using rectangleColor variable 
  rect(0, 0, 100, 1); // Draw rectangle (x, y, width, height)
 
 
}

function windowResized() { //declare function 'windowResized'
  resizeCanvas(windowWidth, windowHeight); //resize the canvas when the window is resized
}

 // Change the color of the rectangle when mouse is pressed
function mousePressed() { // declare function 'mousepressed'
  rectangleColor = color(random(255), random(255), random(255)); //se rectangleColor to RGB=random when mousepressed
}

