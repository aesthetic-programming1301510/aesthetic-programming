# MiniX3

#### The rendered version of my code
![](MiniX3.png)

#### To run the code [click here](https://aesthetic-programming-aesthetic-programming13015-30daab932074f5.gitlab.io/MiniX3/)

#### To read the code [click here](https://gitlab.com/aesthetic-programming1301510/aesthetic-programming/-/blob/main/MiniX3/index.html)

## What I have produced and my thoughts about the process
This week, I kept things simple by creating a basic circular throbber made of 400 rectangles. I have made a throbber that shows progress by the density of the colors using alpha value. It actually reminds me of the throbber used in "Find my iPhone".


The throbber acts like a loading or buffering symbol, and recognizing how annoying it can be to wait, I have made it interactive by adding a feature where clicking (mousePressed()) changes the colors randomly. It's a small thing, but it might make waiting feel less boring.


In the code, The frameCount variable is used to create the looping animation effect. By using the modulo operator %, the code ensures that the animation loops seamlessly. frameRate() is used to control the speed of the animation. A higher frame rate results in smoother animation, while a lower frame rate conserves computational resources.The rotate() is used to animate the rotation of the rectangles in the throbber. By adjusting the rotation angle based on time (using frameCount), the code creates a spinning effect. 


Throbbers are like little signs of progress, letting you know that things are happening behind the scenes. And when changing their looks I think you can convey different feelings or messages.
